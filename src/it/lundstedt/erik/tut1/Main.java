package it.lundstedt.erik.tut1;//this tells the java compiler were this file is in the filesystem
//this is a single-line comment
/*
this is a multi line or inline comment
add a "*" on each new line for good-looking text-comments
* like this
*/
//this is ruffly what the template would look like:
public class Main {	//Main class
	public static void main(String[] args)//main function
	{
		System.out.println("hello java");//prints whatever is inside the() to the terminal and adds a newline
	}//end main function
}//end Main class
zs